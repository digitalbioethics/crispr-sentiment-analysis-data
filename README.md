# CRISPR Sentiment Analysis Data

Underlying and resulting data from a sentiment analysis on Tweets about CRISPR/Cas9: Partial, labelled and anonymised tweet objects, output from the annotation process and resulting machine learning models.

This datasets were produced by the source code available here: https://gitlab.ethz.ch/digitalbioethics/crispr-sentiment-analysis, and allow to reproduce the analysis reported in our paper: https://doi.org/10.2196/17830
